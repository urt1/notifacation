package storage

import (
	"context"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/ptflp/goboilerplate/internal/db"
	"gitlab.com/ptflp/goboilerplate/internal/db/adapter"
	"gitlab.com/ptflp/goboilerplate/internal/infrastructure/db/scanner"
	"gitlab.com/ptflp/goboilerplate/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

//go:generate easytags $GOFILE json

type EmailVerify struct {
	collection db.Collection
	adapter    *adapter.SQLAdapter
}

func NewEmailVerify(sqlAdapter *adapter.SQLAdapter) Verifier {
	return &EmailVerify{adapter: sqlAdapter}
}

func (e *EmailVerify) GetByEmail(ctx context.Context, email, hash string) (models.EmailVerifyDTO, error) {
	var emailVerifies []models.EmailVerifyDTO
	err := e.adapter.List(ctx, &emailVerifies, "email_verify", adapter.Condition{
		Equal: sq.Eq{
			"email":    email,
			"hash":     hash,
			"verified": false,
		},
	})
	if err != nil {
		return models.EmailVerifyDTO{}, err
	}
	if len(emailVerifies) < 1 {
		return models.EmailVerifyDTO{}, fmt.Errorf("email verify %s not found", email)
	}
	return emailVerifies[0], nil
}

func (e *EmailVerify) GetByUserID(ctx context.Context, userID int) (models.EmailVerifyDTO, error) {
	var dto []models.EmailVerifyDTO
	err := e.adapter.List(ctx, &dto, "email_verify", adapter.Condition{
		Equal: sq.Eq{
			"user_id": userID,
		},
	})
	if err != nil {
		return models.EmailVerifyDTO{}, err
	}
	if len(dto) < 1 {
		return models.EmailVerifyDTO{}, fmt.Errorf("email verify with user_id %d not found", userID)
	}
	return dto[0], nil
}

func (e *EmailVerify) Verify(ctx context.Context, userID int) error {
	dto, err := e.GetByUserID(ctx, userID)
	if err != nil {
		return err
	}
	dto.SetVerified(true)
	err = e.adapter.Update(ctx, &dto, adapter.Condition{
		Equal: sq.Eq{
			"id": dto.GetID(),
		},
	}, scanner.Update)

	return err
}

func (e *EmailVerify) VerifyEmail(ctx context.Context, email, hash string) error {
	dto, err := e.GetByEmail(ctx, email, hash)
	if err != nil {
		return err
	}
	dto.SetVerified(true)
	err = e.adapter.Update(ctx, &dto, adapter.Condition{
		Equal: sq.Eq{
			"email":    email,
			"hash":     hash,
			"verified": false,
		},
	}, scanner.Update)

	return err
}

func toDoc(v interface{}) (doc *bson.M, err error) {
	data, err := bson.Marshal(v)
	if err != nil {
		return
	}

	err = bson.Unmarshal(data, &doc)
	return
}

func (e *EmailVerify) Create(ctx context.Context, email, hash string, userID int) error {
	emailVerify := &models.EmailVerifyDTO{}
	emailVerify.Email = email
	emailVerify.Hash = hash
	emailVerify.UserID = userID
	err := e.adapter.Create(ctx, emailVerify)
	return err
}

func (e *EmailVerify) getCollection() *mongo.Collection {
	return e.collection.GetCollection("email_verify")
}
